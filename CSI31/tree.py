# tree.py
# Jonathan Zallas CSI31 Section 2692 09/27/2010
# Program that takes a number of nodes from a user and
#   draws a root tree.

from graphics import *

def main():
    win=GraphWin("Tree",500,500)
    win.setCoords(0,0,500,500)

    # Drawing the interface
    Text(Point(225,80), "Enter number of nodes:").draw(win)
    input = Entry(Point(325,80), 3)
    input.setText("0")
    input.draw(win)
    button=Text(Point(250,40), "Draw Tree")
    button.draw(win)
    Rectangle(Point(200,20), Point(300,60)).draw(win)

    # Drawing the initial point
    p1=Point(250,480)
    p1.draw(win)
    p2=Point(10,100) # Where the bottom of the first line starts

    # Waiting for a mouse click
    win.getMouse()

    # Get nodes from user and draw appropriate number of lines
    nodes = eval(input.getText())

    for i in range (nodes):
        Line(p1, p2).draw(win)
        p2.move(480/(nodes-1), 0)

    # Wait for click and then quit
    button.setText("Click to Exit")
    win.getMouse()
    win.close()
    
main()
