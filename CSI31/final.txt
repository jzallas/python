# Jonathan Zallas CSI31 Section 2692 12/20/2010
# Final Exam

---------------------------------------------------------------
1)CODE
	>>> def average_cost(total_cost, number_of_items):
		return(total_cost/number_of_items)

1)TEST
	>>> average_cost(10,2)
	5.0
---------------------------------------------------------------
2)CODE
	def credit_score():
	    score = eval(input("Please enter a credit score:"))
	    if score >= 300 and score < 620:
	        return("Bad")
	    elif score >= 620 and score < 720:
	        return("Good")
	    elif score >= 720 and score <= 800:
	        return("Excellent")
	    else:
	        return("Invalid")
	def main():
	    print(credit_score())
	main()

2)TEST
	>>> 
	Please enter a credit score:200
	Invalid
	>>> main()
	Please enter a credit score:350
	Bad
	>>> main()
	Please enter a credit score:630
	Good
	>>> main()
	Please enter a credit score:730
	Excellent
---------------------------------------------------------------
3)CODE
	>>> def two_die():
		return(randrange(1,7)+randrange(1,7))

3)TEST
	>>> for k in range(10):
		two_die()
	
	10
	7
	12
	5
	7
	6
	9
	3
	10
	11
---------------------------------------------------------------
4)CODE
	>>> def squares(n):
		squares_list = []
		for k in range(n):
			squares_list.append((k+1)**2)
		return(squares_list)

4)TEST
	>>> for k in range(5):
		squares(k+1)
	
	[1]
	[1, 4]
	[1, 4, 9]
	[1, 4, 9, 16]
	[1, 4, 9, 16, 25]