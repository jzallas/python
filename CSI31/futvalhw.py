# futvalhw.py
# Jonathan Zallas 09/13/2010
#   A program to compute the total accumulation of an investment
#   when you invest a fixed amount every year.

def main():
    print("This program calculates the total accumulation of an investment")
    print("when you incest a fixed amount every year.")

    principal = 0
    investment = eval(input("Enter the initial investment: "))
    apr = eval(input("Enter the annual interest rate: "))
    year = eval(input("Enter the nuumber of years: "))

    for i in range(year):
        principal = (principal + investment)*(1 + apr)

    print("The value in ", year," years is:", principal)

main()
