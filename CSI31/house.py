# house.py
# Jonathan Zallas CSI31 Section 2692 11/07/2010
# Program that draws a simple house.
from graphics import *

def main():

    #Setting up the window and coordinates
    win=GraphWin(500,500,500,500)
    win.setCoords(0,0,130,260)

    #Constructing the walls
    wall1=Polygon(Point(60,70),Point(100,100),Point(100,150),Point(60,120))
    wall1.setFill("Beige")
    wall2=Polygon(Point(60,70),Point(60,120),Point(30,150),Point(30,100))
    wall2.setFill("Beige")

    #Constructing the roof
    roof1=Polygon(Point(60,120),Point(100,150),Point(85,205),Point(45,175))
    roof1.setFill("Brown")
    roof2=Polygon(Point(60,120),Point(45,175),Point(30,150))
    roof2.setFill("Brown")

    #Placing the doors and windows
    door=Polygon(Point(40,90),Point(50,80),Point(50,110),Point(40,120))
    door.setFill("Tan")
    window=Polygon(Point(70,100),Point(80,110),Point(80,120),Point(70,110))
    window.setFill("Light Blue")

    #Drawing the house
    wall1.draw(win)
    wall2.draw(win)
    roof1.draw(win)
    roof2.draw(win)
    door.draw(win)
    window.draw(win)

main()
