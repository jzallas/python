# randomize.py
# Jonathan Zallas CSI31 Section 2692 12/01/2010
# Programming Assignment 6

from random import *

#  This is a simple function which takes a list of n elements
#  and returns a list with the same n elements in a random order.
def shuffle(mylist):

    if len(mylist) > 1:
        
        #Giving the new list the same length as the initial list
        newList = mylist[:]

        #Creating a list with numbers that correspond to the new index
        #and assigns the first element randomly between 0 and n.
        newOrder = []
        newOrder.append(randrange(0, len(mylist)))

        indexAlreadyAssigned = True
        newIndex = 0
        #Fill newOrder with the n elements that correspond to their
        #new indexes
        while len(newOrder) < len(mylist) or indexAlreadyAssigned:
            newIndex = randrange(0, len(mylist))
            if newIndex in newOrder:
                indexAlreadyAssigned = True
            else:
                newOrder.append(newIndex)
                indexAlreadyAssigned = False
                
        #Go through the list of new indexes and put each number in said order
        for k in range(len(mylist)):
            newList[newOrder[k]] = mylist[k]

        return(newList)
            
    # if the list has either zero or one elements, just return the same list
    else:
        return(mylist)
