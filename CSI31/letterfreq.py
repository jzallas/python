# letterfreq.py
# Jonathan Zallas CSI31 Section 2692 12/08/2010
# Programming Assignment 7
# This program analyzes letter frequency in a file
# and prints a report on the frequency of each letter.

def byFreq(pair):
    return pair[1]

#creates a dictionary with the letters of the alphabet and assigns a
#zero count for each letter respectively, returns the new dictionary
def alphabetDict():
	count = {}
	for i in 'abcdefghijklmnopqrstuvwxyz':
	    count[i] = 0
	return(count)

#takes a filename and the alphabet dictionary and prints a table showing
#each letter's frequency in the order of highest to lowest
def printAlphabetFreq(fname, counts):
    items = list(counts.items())
    items.sort()
    items.sort(key=byFreq, reverse=True)
    print()
    print(fname, ":", totalCharacters(counts), "letters in this file.")
    print("{0:<5}{1:>10}{2:>15}".format("Letter", "Freq.", "Rel. Freq."))
    print("-------------------------------")
    for i in range(len(counts)):
        letter, freq = items[i]
        relativeFreq = str((round(freq/totalCharacters(counts),3))*100)+"%"

        print("{0:<5}{1:>10}{2:>15}".format(letter, freq, relativeFreq))

#gets the filename from the user, opens it, and returns both the filename
#and the fileopen
def getFile():
    fname = input("File to analyze: ")
    text = open(fname,'r')
    return(fname, text)

#takes the fileopen and reads each line one by one and converts to lowercase,
#returns the result
def evaluateByLine(text):
    currentLine = text.readline()
    currentLine = currentLine.lower()
    return(currentLine)

#takes the predetermined dictionary and the current line in the file,
#compares it, and if thecharacters are in the line, updates
#each character in the dictionary respectively. returns updated dictionary.
def updateDict(count, currentLine):
    newDict = count.copy()
    for k in currentLine:
        if k in newDict:
            newDict[k] += 1
    return(newDict)

#takes the updated dictionary and counts the total alphabet characters in the
#file, then returns the total
def totalCharacters(someDict):
    total = 0
    for k in someDict:
        total+=someDict[k]
    return(total)
        
def closeFile(text):
    text.close()

def main():
    fileName, fileInput = getFile()
    counts = alphabetDict()
    currentLine = evaluateByLine(fileInput)

    #keep looping until you reach the end of the file
    while currentLine != "":
        counts = updateDict(counts, currentLine)
        currentLine = evaluateByLine(fileInput)

    printAlphabetFreq(fileName, counts)
    closeFile(fileInput)

main()
