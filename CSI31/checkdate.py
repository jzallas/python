# checkdate.py
# Jonathan Zallas CSI31 Section 2692 11/22/2010
# Programming Assignment 5

#  This is a simple function which takes a string that contains a number, evaluates
#  the number (ignoring any zeroes it may start with), and returns an evaluated int.
def evaluate(numstr):
    number = 0
    for k in range(len(numstr)):
        if (eval(numstr[k])!=0) and (eval(numstr[k:]) > number):
            number = eval(numstr[k:])
    return(number)

#  This is a function which takes a string of a date in MM/DD/YYYY format
#  and returns True if it is valid, False if it is invalid.
def checkdate(datestring):
    # Get date and split it into a list of three elements
    datelist = datestring.split("/")

    # Check if more than 3 elements are in the list
    if len(datelist) != 3:
        #Date was not entered in MM/DD/YYYY format.
        return(False)
    else:
        # Check if valid characters (numbers, not letters) were input
        try:
            month = evaluate(datelist[0])
            day = evaluate(datelist[1])
            year = evaluate(datelist[2])

            # Check valid year
            if year <= 0:
                #Year cannot be leess than or equal to zero.
                return(False)
            else:
                # Check valid month
                if month <=0 or month >=13:
                    #Month cannot be less than zero or greater than twelve.
                    return(False)
                else:
                    # Check valid days for April, June, September, and November
                    if month == 4 or month == 6 or month == 9 or month == 11:
                        if day >=31 or day <=0:
                            #This month can only have between 1 and 30 days.
                            return(False)
                        else:
                            return(True)
                    # Check valid days for January, March, May, July, August, October, and December
                    elif month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12:
                        if day >=32 or day <=0:
                            #This month can only have between 1 and 31 days.
                            return(False)
                        else:
                            return(True)
                    # Check valid days for February, but check if it is a leap year first
                    else:
                        if year%4 == 0 and year < 100:
                            leap = True
                        elif year%4 == 0 and year >= 100:
                            if year%100 ==0 and year%400 !=0:
                                leap = False
                            else:
                                leap = True
                        else:
                            leap = False
                            
                        if leap and (day >=30 or day<=0):
                            #This month can only have between 1 and 29 days.
                            return(False)
                        elif (leap == False) and (day>=29 or day<=0):
                            #This month can only have between 1 and 28 days.
                            return(False)
                        else:
                            return(True)
        # Cases where invalid character(non-number) returns an error
        except NameError:
            return(False)
        except SyntaxError:
            return(False)

