# date.py
# Jonathan Zallas CSI31 Section 2692 11/07/2010
# Programming Assignment 4
#  Program that asks user for a date in MM/DD/YYYY format and verifies
#  whether or not it is a valid date.


#  This is a simple function which takes a string that contains a number, evaluates
#  the number (ignoring any zeroes it may start with), and returns an evaluated int.

def evaluate(numstr):
    number = 0
    for k in range(len(numstr)):
        if (eval(numstr[k])!=0) and (eval(numstr[k:]) > number):
            number = eval(numstr[k:])
    return(number)

from graphics import*

def main():
    # Creating the window
    win=GraphWin("Date",500,250)
    win.setCoords(0,0,500,250)

    # Drawing elements in the window
    Text(Point(190,200), "Please enter a date in the format MM/DD/YYYY:").draw(win)
    input = Entry(Point(415,200), 11)
    input.setText("MM/DD/YYYY")
    input.draw(win)
    button=Text(Point(250,150), "Validate")
    button.draw(win)
    Rectangle(Point(200,130), Point (300,170)).draw(win)
    
    # Wait for mouse click
    win.getMouse()

    # Get date and split it into a list of three elements
    date = input.getText()
    datelist = date.split("/")

    # Check if more than 3 elements are in the list
    if len(datelist) != 3:
        Text(Point(250,100),"I'm sorry, your input was invalid:").draw(win)
        Text(Point(250,75),"Date was not entered in MM/DD/YYYY format.").draw(win)
    else:
        # Check if valid characters (numbers, not letters) were input
        try:
            month = evaluate(datelist[0])
            day = evaluate(datelist[1])
            year = evaluate(datelist[2])

            # Check valid year
            if year <= 0:
                Text(Point(250,100),"I'm sorry, your input was invalid:").draw(win)
                Text(Point(250,75),"Year cannot be leess than or equal to zero.").draw(win)
            else:
                # Check valid month
                if month <=0 or month >=13:
                    Text(Point(250,100),"I'm sorry, your input was invalid:").draw(win)
                    Text(Point(250,75),"Month cannot be less than zero or greater than twelve.").draw(win)
                else:
                    # Check valid days for April, June, September, and November
                    if month == 4 or month == 6 or month == 9 or month == 11:
                        if day >=31 or day <=0:
                            Text(Point(250,100),"I'm sorry, your input was invalid:").draw(win)
                            Text(Point(250,75),"This month can only have between 1 and 30 days.").draw(win)
                        else:
                            Text(Point(250,100),"Excellent! Your input was valid.").draw(win)
                    # Check valid days for January, March, May, July, August, October, and December
                    elif month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12:
                        if day >=32 or day <=0:
                            Text(Point(250,100),"I'm sorry, your input was invalid:").draw(win)
                            Text(Point(250,75),"This month can only have between 1 and 31 days.").draw(win)
                        else:
                            Text(Point(250,100),"Excellent! Your input was valid.").draw(win)
                    # Check valid days for February, but check if it is a leap year first
                    else:
                        if year%4 == 0 and year < 100:
                            leap = True
                        elif year%4 == 0 and year >= 100:
                            if year%100 ==0 and year%400 !=0:
                                leap = False
                            else:
                                leap = True
                        else:
                            leap = False
                            
                        if leap and (day >=30 or day<=0):
                            Text(Point(250,100),"I'm sorry, your input was invalid:").draw(win)
                            Text(Point(250,75),"This month can only have between 1 and 29 days.").draw(win)
                        elif (leap == False) and (day>=29 or day<=0):
                            Text(Point(250,100),"I'm sorry, your input was invalid:").draw(win)
                            Text(Point(250,75),"This month can only have between 1 and 28 days.").draw(win)
                        else:
                            Text(Point(250,100),"Excellent! Your input was valid.").draw(win)
        # Cases where invalid character(non-number) returns an error
        except NameError:
            Text(Point(250,100),"I'm sorry, your input was invalid:").draw(win)
            Text(Point(250,75),"You entered invalid characters.").draw(win)
        except SyntaxError:
            Text(Point(250,100),"I'm sorry, your input was invalid:").draw(win)
            Text(Point(250,75),"You entered invalid characters.").draw(win)
    # Wait for mouse click to exit
    button.setText("Click to Exit")
    win.getMouse()
    win.close()
main()
