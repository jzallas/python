# drawsmiley.py
# Jonathan Zallas CSI31 Section 2692 12/12/2010
# Programming Assignment 8
# This is a program that tests the function drawFace by allowing the
# user to click and draw a face infinite times in the window.

from graphics import *
from random import *

# Function that draws a simple smiley face with a pre-determined point,
# radius, and graphics window
def drawFace(center, size, win):

    #head
    radius1 = size
    head = Circle(center, radius1)
    head.setFill("Yellow")
    head.setOutline("Yellow")
    #smile
    radius2 = size/2
    smile1 = Circle(center, radius2)
    recpoint1 = head.getCenter()
    recpoint2 = recpoint1.clone()
    recpoint1.move(-(radius2),0)
    y = round(((radius1**2)-(radius2**2))**.5)
    recpoint2.move((radius2), y)
    smile2 = Rectangle(recpoint1, recpoint2)
    smile2.setOutline("Yellow")
    smile2.setFill("Yellow")
    #eyes
    eye1 = Circle(head.getCenter(), radius1/30)
    eye2 = eye1.clone()
    eye1.move(-(radius2/1.5),(radius2/1.5))
    eye2.move((radius2/1.5),(radius2/1.5))
    eye1.setFill("Black")
    eye2.setFill("Black")
    #draw
    head.draw(win)
    smile1.draw(win)
    smile2.draw(win)
    eye1.draw(win)
    eye2.draw(win)

# Function that gets the (x,y) of a mouse click and returns the point
# as well as a random radius
def getCircleDetails(win):
    p = win.getMouse()
    radius = randrange(10,101)
    return(p, radius)
    
def main():

    win = GraphWin("Face",500,500)
    win.setCoords(0,0,500,500)

    while True:
        center, size = getCircleDetails(win)
        drawFace(center, size, win)
        
        

main()
