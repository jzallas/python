This just contains my assignments written in Python from my relevant courses at Bronx Community College. Should you use any of the code, I recommend you either cite it properly or just do the work yourself because plagiarism is probably not the best way to become a good programmer.

Also, here are the grades I received in each course respectively so you can judge how accurately I know these topics should you have any concerns as to how correct the answers are.

CODE		COURSE NAME				GRADE		TERM
CSCI31	 	Introduction to Computer Programming I	A+		Fall 2010